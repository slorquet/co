#ifndef __CO_NODE_H__
#define __CO_NODE_H__

#include "global.h"

/* CANopen general node commands */

int cmd_canopen_sdo(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_canopen_node(FAR struct co_context_s *ctx, int argc, char **argv);

#endif // __CO_NODE_H__
