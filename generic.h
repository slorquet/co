#ifndef __CO_GENERIC_H__
#define __CO_GENERIC_H__

#include "global.h"

int generic_rxcb(FAR struct co_context_s *ctx, FAR struct can_msg_s *msg);

int cmd_generic_open(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_generic_close(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_generic_status(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_generic_baudrate(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_generic_loopback(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_generic_silent(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_generic_send(FAR struct co_context_s *ctx, int argc, char **argv);
int cmd_generic_receive(FAR struct co_context_s *ctx, int argc, char **argv);

#endif // __CO_GENERIC_H__

