#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <nuttx/drivers/can.h>

#include "canutils/canlib.h"
#include "canutils/libcanopen/sdo.h"
#include "canutils/libcanopen/nmt.h"

#include "global.h"

/* CANopen general node commands */
/* First byte:
 * 0 0 1 0 N N E S mmm dddd - Download Initiate Request  (from client)
 * 0 1 1 0 0 0 0 0 mmm rsvd - Download Initiate Response (from server)
 * 0 1 0 0 0 0 0 0 mmm rsvd - Upload Initiate Request    (from client)
 * 0 1 0 0 N N E S mmm dddd - Upload Initiate Response   (from server)
 *
 * 0 0 0 T N N N C seg-data - Download Segment Request  (from client)
 * 0 0 1 T 0 0 0 0 rsvd     - Download Segment Response (from server)
 * 0 1 1 T 0 0 0 0 rsvd     - Upload Segment Request    (from client)
 * 0 0 0 T N N N C seg-data - Upload Segment Response   (from server)
 */

#define MAXLEN 256
uint8_t sdo_data[MAXLEN];

/* ------------------------------------------------------------------------- */
int canopen_cb_msg(FAR struct co_context_s *ctx, FAR struct can_msg_s *natmsg)
{
  struct canmsg_s msg;

  //dispatch response messages to pending commands
  memcpy(msg.data, natmsg->cm_data, 8);
  msg.dlc = natmsg->cm_hdr.ch_dlc;
  msg.id  = natmsg->cm_hdr.ch_id;

  return canopen_master_rx(&ctx->comaster, &msg);
}

struct cb_dr_ctx_s
{
  bool is_string;
  uint32_t buflen;
  uint32_t done;
  uint8_t* buf;
};

/* ------------------------------------------------------------------------- */
void dump(FAR struct cb_dr_ctx_s *ctx)
{
  bool nonprint = false;
  uint32_t i;

  for (i=0;i<ctx->done;i++)
    {
      if (!ctx->is_string)
        {
          printf("%02X", ctx->buf[i]);
        }
      else if (ctx->buf[i] < 32)
        {
          nonprint = true;
          printf("\\x%02X", ctx->buf[i]);
        }
      else
        {
          printf("%c", ctx->buf[i]);
        }
    }

  printf("\n");
  if(nonprint)
    {
      printf("Note: Some non-printable chars were replaced.\n");
    }
}

/* ------------------------------------------------------------------------- */
int callback_data_received(FAR struct canopen_master_s *master, FAR void *context,
                           uint16_t index, uint8_t subindex,
                           int event, FAR uint8_t *data, uint32_t datalen)
{
  FAR struct cb_dr_ctx_s *ctx = context;

  if (event == SDOCLIENT_CB_EVENT_START)
    {
      printf("Transaction start (id 0x%04X, sub 0x%02X), len=%d\n", index, subindex, datalen);
      if(datalen>256)
        {
          datalen = 256;
        }
      ctx->buflen = datalen;
      ctx->buf = malloc(datalen);
      ctx->done = 0;
    }
  else if (event == SDOCLIENT_CB_EVENT_DATA)
    {
      uint32_t avail;
again:
      avail = ctx->buflen - ctx->done;
      if (avail < datalen)
        {
          //not enough room in buffer to store everything received
          memcpy(ctx->buf+ctx->done, data, avail);
          ctx->done += avail;
          dump(ctx);
          //now proceed with the rest
          ctx->done = 0;
          datalen -= avail;
          goto again;
        }
      else
        {
        //All the received data fits in the buffer
        memcpy(ctx->buf + ctx->done, data, datalen);
        ctx->done += datalen;
        }
    }
  else // SDOCLIENT_CB_EVENT_COMPLETE
    {
      if (ctx->done > 0)
        {
          dump(ctx);
        }
      printf("Transaction complete.\n");
      free(ctx->buf);
    }
  return OK;
}

/* ------------------------------------------------------------------------- */
/* co sdo <nodeid> [exp]get|[exp]put [-string] <index> <subindex> <put:data>*/
int cmd_canopen_sdo(FAR struct co_context_s *ctx, int argc, char **argv)
{
  uint32_t varindex,varsub;
  uint32_t datalen;
  int ret;
  int argi = 1;

  bool is_put = false;
  bool is_exp = false;
  bool is_string = false;

  printf("argc=%d\n", argc);

  set_errno(0);
  ctx->canopen_sdonodeid = strtoul(argv[argi], NULL, 0);
  if(errno)
    {
      fprintf(stderr, "Incorrect node id\n", argv[argi]);
      return ERROR;
    }
  if(ctx->canopen_sdonodeid<1 || ctx->canopen_sdonodeid>127)
    {
      fprintf(stderr, "Incorrect node-id (%u)\n", ctx->canopen_sdonodeid);
      return ERROR;
    }
  fprintf(stderr, "node_id %d\n", ctx->canopen_sdonodeid);
  argi++;

  if(!strcmp(argv[argi], "get"))
    {
      is_put = false;
    }
  else if(!strcmp(argv[argi], "put"))
    {
      is_exp = false;
      is_put = true;
    }
  else if(!strcmp(argv[argi], "eput"))
    {
      is_exp = true;
      is_put = true;
    }
  else
    {
      fprintf(stderr, "Incorrect action %s\n", argv[argi]);
      return ERROR;
    }
  argi++;

  //read string option
  if (!strcmp(argv[argi], "-string"))
    {
      is_string = true;
      argi++;
    }

  //read index
  set_errno(0);
  varindex = strtoul(argv[argi], NULL, 0);
  if(errno)
    {
      fprintf(stderr, "Incorrect object index\n", argv[argi]);
      return ERROR;
    }
  argi++;


  if(varindex > 0xFFFF)
    {
      fprintf(stderr, "Incorrect object index (%u)\n", varindex);
      return ERROR;
    }

  //read subindex
  set_errno(0);
  varsub = strtoul(argv[argi], NULL, 0);
  if(errno)
    {
      fprintf(stderr, "Incorrect object sub-index\n", argv[argi]);
      return ERROR;
    }
  argi++;

  if(varsub > 0xFF)
    {
      fprintf(stderr, "Incorrect object sub-index (%u)\n", varsub);
      return ERROR;
    }

  fprintf(stderr, "index %d subindex %d\n", varindex, varsub);

  if (is_put)
    {
      //sdo download (put)

      char *data;
      int i;

      if (argv[argi]==NULL)
        {
          fprintf(stderr, "Missing data argument\n");
          return ERROR;
        }

      //read data on command line
      data = argv[argi];
      datalen = strlen(argv[argi]);
      if (!is_string)
        {
          //data is hex encoded -> input is hex pairs
          datalen = datalen >> 1;
        }

      //limit

      if (datalen > MAXLEN)
        {
          datalen = MAXLEN;
        }

      if (is_string)
        {
          //direct copy
          memcpy(sdo_data, data, datalen);
        }
      else
        {
          //hex decode

          if (datalen > MAXLEN)
            {
              datalen = MAXLEN;
            }
          for(i=0;i<datalen;i++)
            {
              int tmp,conv;
              conv = sscanf(data, "%2x", &tmp);
              printf("conv: %s %d\n",data,tmp);
              if(conv != 1)
                {
                  fprintf(stderr, "hex conversion error (byte %d, conv=%d, errno=%d)\n",i,conv,errno);
                  return ERROR;
                }
              sdo_data[i] = tmp;
              data += 2;
            }
        }

      printf("datalen %d data[0..3] %02X%02X%02X%02X\n", datalen, 
             sdo_data[0], sdo_data[1], sdo_data[2], sdo_data[3] );

      //sanity check

      if(datalen>4 && is_exp)
        {
          fprintf(stderr, "Data is too long (%d) for expedited transfer\n", datalen);
          return ERROR;
        }

      ret = canopen_sdoclient_download(&ctx->comaster, ctx->canopen_sdonodeid, 
                                       varindex, varsub,
                                       is_exp, sdo_data, datalen);
    }
  else
    {
      //sdo upload (get)
      ret = canopen_sdoclient_upload(&ctx->comaster, ctx->canopen_sdonodeid, varindex, varsub, callback_data_received, ctx);
    }

  if(ret<0)
    {
      fprintf(stderr, "CAN transaction failed, errno=%d\n",ret);
      return ERROR;
    }

  return ret;
}

/* ------------------------------------------------------------------------- */
/* co node <nodeid>|all start|stop|preop|rstapp|rstcomm */
int cmd_canopen_node(FAR struct co_context_s *ctx, int argc, char **argv)
{
  int argi = 1;
  int nodeid;

  if (!strcmp(argv[argi], "all"))
    {
      nodeid = 0;
    }
  else
    {
      set_errno(0);
      nodeid = strtoul(argv[argi], NULL, 0);
      if(errno)
        {
          fprintf(stderr, "Incorrect node id\n", argv[argi]);
          return ERROR;
        }
      if(nodeid<1 || nodeid>127)
        {
          fprintf(stderr, "Incorrect node-id (%u)\n", ctx->canopen_sdonodeid);
          return ERROR;
        }
    }
  argi++;

  if (!strcmp(argv[argi], "start"))
    {
      return canopen_nmtmaster_start(&ctx->comaster, nodeid);
    }
  else if (!strcmp(argv[argi], "stop"))
    {
      return canopen_nmtmaster_stop(&ctx->comaster, nodeid);
    }
  else if (!strcmp(argv[argi], "preop"))
    {
      return canopen_nmtmaster_preop(&ctx->comaster, nodeid);
    }
  else if (!strcmp(argv[argi], "rstapp"))
    {
      return canopen_nmtmaster_resetapp(&ctx->comaster, nodeid);
    }
  else if (!strcmp(argv[argi], "rstcomm"))
    {
      return canopen_nmtmaster_resetcomm(&ctx->comaster, nodeid);
    }
  else
    {
      fprintf(stderr, "Unknown command : %s\n", argv[argi]);
    }
  return OK;
}

