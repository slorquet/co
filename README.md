co - CANopen toolbox

This software is a debug tool to explore the behaviour of CANopen devices.

What it can do:

Generic commands

* select current bus
* show packets on bus
* select baudrate/loopback
* send arbitray CAN packet

CANopen Master commands

* start/stop NMT remote slaves
* start/stop node guarding
* modify contents of remote node Object Directory by SDO
* display status information

It can be used as a basic CAN and CANopen tester.

The software is structured as follows:

* foreground task gets executed by nsh, sends command to a background task that can keep the bus opened, and quits immediately.
* background task gets started at first command and executes commands sent by foreground task without quitting, this is used to keep the bus opened.
* the open command starts a background thread that listens to can messages and dispatch them as required. This thread is killed by the close command.

Online help:
```
co open <device>
   close
   status
   br <baudrate>
   loopback on|off (receive own packets)
   silent on|off (disable tx)
   tx <canid11> <data>
   etx <canid29> <data>
   rx on|off (default on)
   sdo <[0x]nodeid> get <[0x]index> <[0x]subindex>
   sdo <[0x]nodeid> put <[0x]index> <[0x]subindex> <hexdata>
   sdo <[0x]nodeid> eput <[0x]index> <[0x]subindex> <hexdata> (expedited)
   node all|<[0x]nodeid> start|stop
```