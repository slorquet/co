#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <nuttx/drivers/can.h>

#include "canutils/canlib.h"
#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/master.h"

#include "global.h"

/****************************************************************************
 * Global variables
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/* ------------------------------------------------------------------------- */
static int txcb(FAR void *priv, FAR struct canmsg_s *msg)
{
  FAR struct co_context_s *privdata = (FAR struct co_context_s*)priv;
  struct can_msg_s nmsg;
  nmsg.cm_hdr.ch_dlc = msg->dlc;
  nmsg.cm_hdr.ch_id  = msg->id;
  *((uint64_t*)nmsg.cm_data) = *((uint64_t*)msg->data);
  if(write(privdata->can_fd, &nmsg, CAN_MSGLEN(msg->dlc)))
    {
      return -errno;
    }
  return OK;
}

/* ------------------------------------------------------------------------- */
void* task_rxmsg(FAR void *arg)
{
  int ret;
  FAR struct co_context_s *ctx = arg;
  struct can_msg_s message;

  printf("Start CAN msg rx\n");
  ctx->can_running = true;
  while(ctx->can_running)
    {
      ret = read(ctx->can_fd, &message, sizeof(message));
printf("co can read returns %d bytes",ret);
      if(ret>0)
        {
          if(ctx->can_rxcb)
            {
              ctx->can_rxcb(ctx, &message);
            }
        }
    }
  printf("End CAN msg rx\n");
  return NULL;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_open(FAR struct co_context_s *ctx, int argc, char **argv)
{
  int fd;
  FAR char *dev = argv[1];

  if(ctx->can_fd>0)
    {
      fprintf(stderr, "CAN device (%s) already opened\n", ctx->can_devname);
      return ERROR;
    }

  if(argc<1)
    {
      fprintf(stderr, "No device to open\n");
      return ERROR;
    }

  fd = open(dev, O_RDWR);
  if(fd<0)
    {
      fprintf(stderr, "Cannot open %s, errno=%d\n", dev, errno);
      return ERROR;
    }

  ctx->can_fd = fd;
  ctx->can_devname = strdup(dev);

  canopen_master_init(&ctx->comaster, txcb, ctx);

  //start rx thread
  if(pthread_create(&ctx->can_thread, NULL, task_rxmsg, ctx)!=OK)
    {
      fprintf(stderr, "Failed to create rx thread, errno=%d\n", errno);
      return ERROR;
    }
  
  return OK;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_close(FAR struct co_context_s *ctx, int argc, char **argv)
{
  int ret;

  if(ctx->can_fd<0)
    {
      fprintf(stderr, "No CAN device opened\n");
      return ERROR;
    }

  ret = canopen_master_stop(&ctx->comaster);
  if(ret)
    {
      fprintf(stderr, "WARNING: CANopen node failed to stop, err=%d\n", ret);
    }

  //stop the rx thread
  ctx->can_running = false;
  pthread_kill(ctx->can_thread, SIGUSR1);

  close(ctx->can_fd);
  ctx->can_fd = -1;
  free(ctx->can_devname);

  return OK;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_kill(FAR struct co_context_s *ctx, int argc, char **argv)
{
  return OK;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_status(FAR struct co_context_s *ctx, int argc, char **argv)
{
  bool loop   = FALSE;
  bool silent = FALSE;
  int baud;

  if(ctx->can_fd<0)
    {
      printf("CAN device not opened\n");
      return OK;
    }

  if(canlib_getloopback(ctx->can_fd, &loop))
    {
      printf("CAN device not accessible (loopback mode)\n");
      return OK;
    }

  if(canlib_getsilent(ctx->can_fd, &loop))
    {
      printf("CAN device not accessible (silent mode)\n");
      return OK;
    }

  if(canlib_getbaud(ctx->can_fd, &baud))
    {
      printf("CAN device not accessible (baud rate)\n");
      return OK;
    }

  printf("CAN device %s opened as fd %d\n", ctx->can_devname, ctx->can_fd);
  printf("Baud rate: %d bps\n", baud);
  printf("Loopback: %s Silent: %s\n", loop?"Yes":"No", silent?"Yes":"No");

  return OK;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_baudrate(FAR struct co_context_s *ctx, int argc, char **argv)
{
  int bauds;
  if(ctx->can_fd<0)
    {
      fprintf(stderr, "CAN device not opened\n");
      return ERROR;
    }
  if(argc!=2)
    {
      fprintf(stderr, "Bad arguments (%d expect 1)\n", argc-1);
      return ERROR;
    }

  set_errno(0);
  bauds = strtol(argv[1], NULL, 0);
  if(errno != 0)
    {
      fprintf(stderr, "Invalid baudrate '%s'\n", argv[1]);
      return ERROR;
    }

  if(canlib_setbaud(ctx->can_fd, bauds)!=0)
    {
      fprintf(stderr, "Unable to set baud rate, errno=%d\n", errno);
    }

  return OK;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_loopback(FAR struct co_context_s *ctx, int argc, char **argv)
{
  int ret = OK;

  if(argc!=2)
    {
      fprintf(stderr, "Incorrect arguments\n");
      return ERROR;
    }

  if(!strcmp(argv[1],"on"))
    {
      ret = canlib_setloopback(ctx->can_fd, TRUE);
    }
  else if(!strcmp(argv[1],"off"))
    {
      ret = canlib_setloopback(ctx->can_fd, FALSE);
    }
  else
    {
      fprintf(stderr, "Incorrect argument\n");
      return ERROR;
    }

  if(ret != OK)
    {
      fprintf(stderr, "Unable to set loopback mode, errno=%d\n", errno);
    }

  return ret;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_silent(FAR struct co_context_s *ctx, int argc, char **argv)
{
  int ret = OK;

  if(argc!=2)
    {
      fprintf(stderr, "Incorrect arguments\n");
      return ERROR;
    }

  if(!strcmp(argv[1],"on"))
    {
      ret = canlib_setsilent(ctx->can_fd, TRUE);
    }
  else if(!strcmp(argv[1],"off"))
    {
      ret = canlib_setsilent(ctx->can_fd, FALSE);
    }
  else
    {
      fprintf(stderr, "Incorrect argument\n");
      return ERROR;
    }

  if(ret != OK)
    {
      fprintf(stderr, "Unable to set silent mode, errno=%d\n", errno);
    }

  return ret;
}

/* ------------------------------------------------------------------------- */
int cmd_generic_send(FAR struct co_context_s *ctx, int argc, char **argv)
{
  bool extended;
  uint32_t id;
  uint32_t len;
  uint32_t i,j;
  struct can_msg_s msg;
  uint32_t byte;
  char buf[3];
  FAR char *hexdata;
  int ret;

  extended = !strcmp(argv[0],"etx");

#ifndef CONFIG_CAN_EXTID
  if(extended)
    {
      fprintf(stderr, "CONFIG_CAN_EXTID is not enabled.\n");
      return ERROR;
    }
#endif

  if(argc!=3)
    {
      fprintf(stderr, "Incorrect arguments\n");
      return ERROR;
    }

  set_errno(0);
  id = strtoul(argv[1], NULL, 0);
  if(errno != 0)
    {
      fprintf(stderr, "Invalid CAN ID '%s'\n", argv[1]);
      return ERROR;
    }

   if(id > (extended?CAN_MAX_EXTMSGID:CAN_MAX_STDMSGID) )
     {
       fprintf(stderr, "Incorrect CAN ID\n");
       return ERROR;
     }

  hexdata = argv[2];
  len = strlen(hexdata) >> 1;
  if(len>8)
     {
       fprintf(stderr, "Message too long (max 8 bytes)\n");
       return ERROR;
     }


#ifdef CONFIG_CAN_EXTID
  msg.cm_hdr.ch_extid = extended;
#endif
  msg.cm_hdr.ch_id    = id;
  msg.cm_hdr.ch_rtr   = 0;
  msg.cm_hdr.ch_dlc   = len;

  buf[2] = 0;
  j=0;
  for(i=0; i<len; i++)
    {
      buf[0] = hexdata[j++];
      buf[1] = hexdata[j++];
      set_errno(0);
      byte = strtoul(buf, NULL, 16);
      if(errno!=0)
        {
          fprintf(stderr, "Incorrect hex value '%s' for byte #%d\n", buf, i);
          return ERROR;
        }
    msg.cm_data[i] = byte;
    }

  /* send */
  ret = write(ctx->can_fd, &msg, CAN_MSGLEN(len));
  if(ret<0)
    {
      fprintf(stderr, "Write to CAN bus failed, errno=%d\n", errno);
      return ERROR;
    }

  return OK;
}

/* ------------------------------------------------------------------------- */
int generic_rxcb(FAR struct co_context_s *ctx, FAR struct can_msg_s *msg)
{
  int i;

#ifdef CONFIG_CAN_ERRORS
  if(msg->cm_hdr.ch_error)
    {
      /* This is not a message but an error report */
      printf("ERROR %08X\n", msg->cm_hdr.ch_id);
      return OK;
    }
#endif

#ifdef CONFIG_CAN_EXTID
  if(msg->cm_hdr.ch_extid)
    {
      printf("EXT %08X", msg->cm_hdr.ch_id);
    }
  else
    {
      printf("STD      %03X", msg->cm_hdr.ch_id);
    }
#else
    printf("%03X", msg->cm_hdr.ch_id);
#endif

  printf(" [%2d]", msg->cm_hdr.ch_dlc);

  if(msg->cm_hdr.ch_rtr)
    {
      printf(" RTR");
    }
  else
    {
      for(i=0;i<msg->cm_hdr.ch_dlc;i++)
        {
          printf(" %02X", msg->cm_data[i]);
        }
    }

  printf("\n");

  //message is displayed -> forward to canopen layer
  return canopen_cb_msg(ctx,msg);
}

/* ------------------------------------------------------------------------- */
int cmd_generic_receive(FAR struct co_context_s *ctx, int argc, char **argv)
{
  if(argc!=2)
    {
      fprintf(stderr, "Incorrect arguments\n");
      return ERROR;
    }

  if(!strcmp(argv[1],"on"))
    {
      if(ctx->can_rxcb == generic_rxcb)
        {
          fprintf(stderr, "Packet reception already enabled\n");
          return ERROR;
        }
      ctx->can_rxcb = generic_rxcb;
    }
  else if(!strcmp(argv[1],"off"))
    {
      if(ctx->can_rxcb == NULL)
        {
          fprintf(stderr, "Packet reception already disabled\n");
          return ERROR;
        }
      ctx->can_rxcb = NULL;
    }
  else
    {
      fprintf(stderr, "Incorrect argument\n");
      return ERROR;
    }
  return OK;
}

/* ------------------------------------------------------------------------- */

