/****************************************************************************
 * canutils/co/co_main.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <semaphore.h>
#include <nuttx/drivers/can.h>

#include "utils.h"

#include "global.h"
#include "generic.h"
#include "node.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private types
 ****************************************************************************/

/****************************************************************************
 * App global variables
 ****************************************************************************/

static struct command_s commands[] = 
{
  /* General CAN commands */

  {"open"    , cmd_generic_open},
  {"close"   , cmd_generic_close},
  {"status"  , cmd_generic_status},
  {"br"      , cmd_generic_baudrate},
  {"tx"      , cmd_generic_send},
  {"etx"     , cmd_generic_send},
  {"rx"      , cmd_generic_receive},
  {"loopback", cmd_generic_loopback},
  {"silent"  , cmd_generic_silent},

  /* CANopen node commands */

  {"node"    , cmd_canopen_node},
  {"sdo"     , cmd_canopen_sdo},
};

#define COMMANDS_COUNT (sizeof(commands)/sizeof(commands[0]))

static struct co_context_s co_context = 
{
  .task_started = false,
  .task_running = false,
  .cmd_txsem    = SEM_INITIALIZER(0),
  .cmd_rxsem    = SEM_INITIALIZER(0),
  .cmd_fn       = NULL,
  .can_fd       = -1,
  .can_devname  = NULL,
  .can_rxcb     = generic_rxcb,
};

/****************************************************************************/
static void usage(void)
{
  fprintf(stderr, 
  "co open <device>\n"
  "   close\n"
  "   status\n"
  "   br <baudrate>\n"
  "   loopback on|off (receive own packets)\n"
  "   silent on|off (disable tx)\n"
  "   tx <canid11> <data>\n"
  "   etx <canid29> <data>\n"
  "   rx on|off (default on)\n"
  "   sdo <[0x]nodeid> get <[0x]index> <[0x]subindex>\n"
  "   sdo <[0x]nodeid> put <[0x]index> <[0x]subindex> <hexdata>\n"
  "   sdo <[0x]nodeid> eput <[0x]index> <[0x]subindex> <hexdata> (expedited)\n"
  "   node all|<[0x]nodeid> start|stop\n"
);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/


/* ------------------------------------------------------------------------- */
/* this task just waits for commands and messages */
int task_background(int s_argc, char **s_argv)
{
  FAR struct co_context_s *ctx = &co_context;

  printf("In CAN task\n");

  ctx->task_running = true;
  while(ctx->task_running)
    {
      int ret = sem_wait(&ctx->cmd_txsem);

      if(ret==OK)
        {
          if(ctx->cmd_fn)
            {
              printf("CAN task command\n");
              ctx->cmd_ret = ctx->cmd_fn(ctx, ctx->cmd_argc,ctx->cmd_argv);
              ctx->cmd_fn = NULL;
              sem_post(&ctx->cmd_rxsem);
            }
          else
            {
              fprintf(stderr, "No command, stopping task.\n");
              break;
            }
        }
    }
  printf("CAN task done\n");
  return 0;
}

/* ------------------------------------------------------------------------- */
int task_post_command(FAR struct co_context_s *ctx, co_fn_t fn, int argc, FAR char **argv)
{
  if (!ctx->task_started)
    {
      int pid;
      printf("Starting task...\n");
      ctx->task_running = false;
      ctx->task_started = true;
      pid = task_create("cotask", SCHED_PRIORITY_MAX, 1024, task_background, NULL);
      printf("Started pid %d...\n", pid);
      while(!ctx->task_running) {}
      printf("Running.\n");
    }

  ctx->cmd_fn   = fn;
  ctx->cmd_argc = argc;
  ctx->cmd_argv = argv;
  sem_post(&ctx->cmd_txsem);
  sem_wait(&ctx->cmd_rxsem);
  return ctx->cmd_ret;
}

/****************************************************************************
 * co_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int co_main(int argc, FAR char *argv[])
#endif
{
  int i;
  FAR char* command;

  if(argc < 2)
    {
      usage();
      return 1;
    }
  command = argv[1];

  for (i=0;i<COMMANDS_COUNT;i++)
    {
      if (!strcmp(commands[i].name, command))
        {
          return task_post_command(&co_context, commands[i].fn, argc-1, argv+1);
        }
    }

  fprintf(stderr, "Unknown command '%s'\n", command);
  return ERROR;
}

