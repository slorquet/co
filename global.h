#ifndef __CO_GLOBAL_H__
#define __CO_GLOBAL_H__

#include <stdbool.h>
#include <semaphore.h>
#include <pthread.h>
#include <nuttx/drivers/can.h>

#include "canutils/libcanopen/master.h"

struct co_context_s;

typedef int (*co_fn_t)(FAR struct co_context_s *ctx, int argc, char **argv);
typedef int (*co_rxfn_t)(FAR struct co_context_s *ctx, FAR struct can_msg_s *msg);

struct co_context_s
{
  bool       task_started;
  bool       task_running;
  sem_t      cmd_txsem;  //asserted when foreground process wants to send a command
  sem_t      cmd_rxsem;  //asserted when background task has finished command execution
  co_fn_t    cmd_fn;   //pending command
  int        cmd_argc; //pending command args
  FAR char **cmd_argv; //pending command args
  int        cmd_ret;  //pending command retval

//generic
  int        can_fd;      //can device
  pthread_t  can_thread;
  bool       can_running; //thread run flag
  FAR char * can_devname; //can device name, managed by open/close
  co_rxfn_t  can_rxcb;    //can callback func

  struct canopen_master_s comaster;
  int canopen_sdonodeid;

};

struct command_s
{
  char *name;
  co_fn_t fn;
};

int canopen_cb_msg(FAR struct co_context_s *ctx, FAR struct can_msg_s *msg); //callback when a canopen msg is received

#endif // __CO_GLOBAL_H__

